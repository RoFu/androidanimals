package au.edu.tafensw.roland.animals;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Roland Fuller
 * Show a list of animal images to select from.
 */
public class AnimalActivity extends AppCompatActivity {

    private static final int CHOOSE_CODE = 1234;
    private static List<Animal> animals;

    /**
     * Declared statically so that the animal objects themselves are fixed.
     * Ideally, all this data should be loaded in from an XML or other data store
     */
    static {
        animals = new ArrayList<>();
        animals.add(new Animal(R.string.alpaca, R.drawable.alpaca));
        animals.add(new Animal(R.string.bear, R.drawable.bear));
        animals.add(new Animal(R.string.cat, R.drawable.cat));
        animals.add(new Animal(R.string.dog, R.drawable.dog));
        animals.add(new Animal(R.string.donkey, R.drawable.donkey));
        animals.add(new Animal(R.string.duck, R.drawable.duck));
        animals.add(new Animal(R.string.eagle, R.drawable.eagle));
        animals.add(new Animal(R.string.elephant, R.drawable.elephant));
        animals.add(new Animal(R.string.fox, R.drawable.fox));
        animals.add(new Animal(R.string.frog, R.drawable.frog));
        animals.add(new Animal(R.string.giraffe, R.drawable.giraffe));
        animals.add(new Animal(R.string.hippopotamus, R.drawable.hippopotamus));
        animals.add(new Animal(R.string.koala, R.drawable.koala));
        animals.add(new Animal(R.string.lion, R.drawable.lion));
        animals.add(new Animal(R.string.monkey, R.drawable.monkey));
        animals.add(new Animal(R.string.panda, R.drawable.panda));
        animals.add(new Animal(R.string.pig, R.drawable.pig));
        animals.add(new Animal(R.string.rabbit, R.drawable.rabbit));
        animals.add(new Animal(R.string.rhino, R.drawable.rhino));
        animals.add(new Animal(R.string.seal, R.drawable.seal));
        animals.add(new Animal(R.string.snake, R.drawable.snake));
        animals.add(new Animal(R.string.squirrel, R.drawable.squirrel));
    }

    /**
     * When the activity is created
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal); // display the correct activity GUI

        // Get a handle on the ListView from the GUI
        ListView animalList = findViewById(R.id.animalList);
        // Create an adapter and set the list to display from this adapter
        AnimalAdapter adapter = new AnimalAdapter(this, animals);
        animalList.setAdapter(adapter);

        // For each animal in the listView, attach the same event handler
        animalList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Grab out the animal at that particular clicked position in the list
                Animal animal = (Animal) parent.getItemAtPosition(position);
                // Create an intent with the animal's data.
                // Thsi could also be accomplished by serialising the whole animal object
                // to send to the next activity
                Intent intent = new Intent(getApplicationContext(), ChooseActivity.class);
                intent.putExtra(MainActivity.IMAGE_CODE, animal.getImage());
                intent.putExtra(MainActivity.ANIMAL_NAME, animal.getName());
                startActivityForResult(intent, CHOOSE_CODE);
            }
        });

        // get a handle on the person name textview and set it to the one we were passed from the MainActivity
        TextView personName = findViewById(R.id.personName);
        Person person = (Person) getIntent().getExtras().getSerializable(MainActivity.PERSON);
        personName.setText(person.name);
    }

    /**
     * The activity result handler - called when the called activity returns
     *
     * @param requestCode a unique identifier for the particular activity that was called upon
     * @param resultCode  Whether or not the activity succeeded
     * @param returnData  An intent which may contain result data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent returnData) {
        if (requestCode == CHOOSE_CODE && resultCode == RESULT_OK) {
            if (returnData.hasExtra(MainActivity.SCORE)) {
                // update our score according to whether the other activity said so
                TextView score = findViewById(R.id.score);
                int scoreNum = Integer.parseInt(score.getText().toString());
                scoreNum += returnData.getIntExtra(MainActivity.SCORE, 0);
                score.setText(String.valueOf(scoreNum));
            }
        }
    }
}
