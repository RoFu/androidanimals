package au.edu.tafensw.roland.animals;

import java.io.Serializable;

/**
 * Just a boring data class.
 * Note that it must be serializable in this case as we wish to send this type of object
 * through the intent mechanism
 */
public class Person implements Serializable {
    public final String name;
    public final int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }
}