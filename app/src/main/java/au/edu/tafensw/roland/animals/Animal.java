package au.edu.tafensw.roland.animals;

/**
 * Created by Roland Fuller
 * A simple data class which correlates a name to an image
 */
public class Animal {
    public static int[] ANIMAL_NAMES = {R.string.alpaca,
            R.string.bear,
            R.string.cat,
            R.string.dog,
            R.string.donkey,
            R.string.eagle,
            R.string.elephant,
            R.string.fox,
            R.string.frog,
            R.string.giraffe,
            R.string.hippopotamus,
            R.string.koala,
            R.string.lion,
            R.string.monkey,
            R.string.panda,
            R.string.pig,
            R.string.rabbit,
            R.string.rhino,
            R.string.seal,
            R.string.snake,
            R.string.squirrel};

    private int name;
    private int image;

    public Animal(int name, int image) {
        this.name = name;
        this.image = image;
    }

    public int getImage() {
        return image;
    }

    public int getName() {
        return name;
    }
}
