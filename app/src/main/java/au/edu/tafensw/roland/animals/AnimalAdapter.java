package au.edu.tafensw.roland.animals;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import java.util.List;

/**
 * Created by Roland Fuller
 */
public class AnimalAdapter extends ArrayAdapter<Animal> {

    /**
     * A constructor which allows us to pass in the data source
     *
     * @param context
     * @param animals
     */
    public AnimalAdapter(Context context, List<Animal> animals) {
        // load a particular layout to display
        super(context, R.layout.animal_layout, animals);
    }

    /**
     * This method is called by the View which has this adapter attached to it,
     * in order to obtain the data to display at a given position in the list
     *
     * @param position
     * @param convertView
     * @param parentViewGroup
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parentViewGroup) {
        // create the contents of the list view entry
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.animal_layout, parentViewGroup, false);
        ImageView imageView = rowView.findViewById(R.id.image);
        imageView.setImageResource(getItem(position).getImage());
        return rowView;
    }
}
