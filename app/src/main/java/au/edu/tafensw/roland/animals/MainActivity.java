package au.edu.tafensw.roland.animals;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Roland Fuller
 * An entry point to the application.
 * Loads up person data xml file and then launches to the game activity
 * with the first person record. This is just a demonstration of working with XML
 * data and passing around objects.
 */
public class MainActivity extends AppCompatActivity {

    public static final String SCORE = "SCORE";
    public static final String IMAGE_CODE = "IMAGE_CODE";
    public static final String ANIMAL_NAME = "ANIMAL_NAME";
    public static final String PERSON = "PERSON";

    // We don't use namespaces
    private static final String ns = null;

    /**
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        List<Person> result = null;

        // TIP: return a list of all files available in assets directory: getAssets().list("")
        try {
            InputStream is = getAssets().open("personData.xml"); // remember asset files are read only
            result = parse(is); // parse the file and hopefully get the data in the form of objects
        } catch (IOException e) {
            // File not found or unable to open
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            // unable to parse xml file contents
            e.printStackTrace();
        }

        // Create an intent to go to the game activity
        Intent intent = new Intent(this, AnimalActivity.class);
        intent.putExtra(PERSON, result.get(0)); // put the whole person object in the intent extra data - MUST be serializable
        startActivity(intent);
    }

    /**
     * An unused method which shows how you can translate object data back into XML
     *
     * @param result
     */
    private void writeToXML(List<Person> result) {
        String XMLyPerson = "";
        for (Object o : result) {
            Person p = (Person) o;
            try {
                XMLyPerson += createXMLPerson(p);
            } catch (Exception ex) {
            }
        }
    }

    /**
     * Remember assets are read-only, so if we want to store and manipulate XML data files
     * they must go to the application usable disk space
     *
     * @param fileName
     * @param xml
     */
    private void saveXMLToDisk(String fileName, String xml) {
        try {
            FileOutputStream fos = openFileOutput(fileName, Context.MODE_PRIVATE);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fos);
            outputStreamWriter.write(xml);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // The following methods all relate to parsing an XML data file.
    // There is a lot of improvements that could be made by splitting these off into a separate class
    // and using inheritance from that class to write specific code to parse and generate
    // different kinds of XML files. Left as an exercise to the student.

    /**
     * Try to parse an XML file and return a list of objects understood from the contents
     *
     * @param in
     * @return A list of objects read from the file
     * @throws XmlPullParserException
     * @throws IOException
     */
    public List parse(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readPeople(parser);
        } finally {
            in.close();
        }
    }

    /**
     * Block to process a "people" section of the file
     *
     * @param parser
     * @return
     * @throws XmlPullParserException
     * @throws IOException
     */
    private List readPeople(XmlPullParser parser) throws XmlPullParserException, IOException {
        List<Person> entries = new ArrayList<Person>();

        parser.require(XmlPullParser.START_TAG, ns, "people");

        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            // Starts by looking for the person tag
            if (name.equals("person")) {
                entries.add(readPerson(parser));
            } else {
                skip(parser);
            }
        }
        return entries;
    }

    /**
     * Parses the contents of a person. If it encounters a name or age tag, hands them off
     * to their respective "read" methods for processing. Otherwise, skips the tag.
     *
     * @param parser
     * @return
     * @throws XmlPullParserException
     * @throws IOException
     */
    private Person readPerson(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "person");
        int age = Integer.parseInt(parser.getAttributeValue(null, "age"));
        String name = null;
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String tagName = parser.getName();
            if (tagName.equals("name")) {
                name = readName(parser);
            } else {
                skip(parser);
            }
        }
        return new Person(name, age);
    }

    /**
     * Processes title tags in the feed.
     *
     * @param parser
     * @return
     * @throws IOException
     * @throws XmlPullParserException
     */
    private String readName(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "name");
        String title = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "name");
        return title;
    }

    /**
     * For the tags title and summary, extracts their text values.
     *
     * @param parser
     * @return
     * @throws IOException
     * @throws XmlPullParserException
     */
    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    /**
     * Skip past an XML block
     *
     * @param parser
     * @throws XmlPullParserException
     * @throws IOException
     */
    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }

    /**
     * This could be a static utility method in the Person class
     * Generates person XML data from a Person object
     *
     * @param person
     * @return
     * @throws IllegalArgumentException
     * @throws IllegalStateException
     * @throws IOException
     */
    public String createXMLPerson(Person person) throws IllegalArgumentException, IllegalStateException, IOException {
        XmlSerializer xmlSerializer = Xml.newSerializer();
        StringWriter stringWriter = new StringWriter();
        xmlSerializer.setOutput(stringWriter);

        xmlSerializer.startDocument("UTF-8", true);
        xmlSerializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);

        xmlSerializer.startTag(ns, "person");
        xmlSerializer.attribute(ns, "age", String.valueOf(person.age));

        xmlSerializer.startTag(ns, "name");
        xmlSerializer.text(person.name);
        xmlSerializer.endTag(ns, "name");

        xmlSerializer.endTag(ns, "person");

        xmlSerializer.endDocument();
        return stringWriter.toString();
    }

}