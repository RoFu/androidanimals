package au.edu.tafensw.roland.animals;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.util.Random;

import static au.edu.tafensw.roland.animals.Animal.ANIMAL_NAMES;
import static au.edu.tafensw.roland.animals.MainActivity.ANIMAL_NAME;
import static au.edu.tafensw.roland.animals.MainActivity.IMAGE_CODE;
import static au.edu.tafensw.roland.animals.MainActivity.SCORE;

/**
 * Created by Roland Fuller
 * Simple activity which displays a chosen animal picture and generates some buttons
 * programmatically to select the name of the animal. Reports back to the caller the score
 * obtained or a failure code if too many guesses.
 */
public class ChooseActivity extends AppCompatActivity {

    private int fails = 0;
    private Button[] buttons = new Button[4];
    /**
     * The handler attached to the correct answer button
     */
    private View.OnClickListener correctListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent returnData = getIntent();
            returnData.putExtra(SCORE, 1);
            setResult(RESULT_OK, returnData);
            finish();
        }
    };
    /**
     * The handler attached to all other incorrect buttons
     */
    private View.OnClickListener incorrectListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            view.setEnabled(false);
            fails++;
            if (fails > 2) {
                setResult(RESULT_CANCELED);
                finish();
            }
        }
    };

    /**
     * Load up the GUI file, generate the buttons
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose);

        Intent sender = getIntent();

        ImageView animalImage = findViewById(R.id.animalImage);
        animalImage.setImageResource(sender.getIntExtra(IMAGE_CODE, 0));

        buttons[0] = findViewById(R.id.button1);
        buttons[1] = findViewById(R.id.button2);
        buttons[2] = findViewById(R.id.button3);
        buttons[3] = findViewById(R.id.button4);

        for (Button b : buttons) {
            b.setOnClickListener(incorrectListener);
            b.setText(ANIMAL_NAMES[new Random().nextInt(ANIMAL_NAMES.length)]);
        }

        int correctIndex = new Random().nextInt(3);
        buttons[correctIndex].setText(sender.getIntExtra(ANIMAL_NAME, 0));
        buttons[correctIndex].setOnClickListener(correctListener);
    }

}
